CC = x86_64-w64-mingw32-gcc

.PHONY : all
all : d.efi e.efi

d.efi : d.c
	$(CC) -shared -nostdlib -e EfiMain@8 -mno-red-zone -mno-sse -nostdinc -s -O -ffreestanding -fno-builtin -fno-stack-protector -fno-strict-aliasing -IEdkCompatibilityPkg/Foundation/Efi/Include/ -IEdkCompatibilityPkg/Foundation/Framework/Include/ -IEdkCompatibilityPkg/Foundation/Include/ -IEdkCompatibilityPkg/Foundation/Efi -IEdkCompatibilityPkg/Foundation/Include/X64 -o d.dll d.c
	(dd bs=1 count=220 && perl -e 'printf "\xc"' && dd bs=1 count=1 of=/dev/null && cat) < d.dll > d.efi

e.efi : e.c
	$(CC) -shared -nostdlib -e EfiMain@8 -mno-red-zone -mno-sse -nostdinc -s -O -ffreestanding -fno-builtin -fno-stack-protector -fno-strict-aliasing -IEdkCompatibilityPkg/Foundation/Efi/Include/ -IEdkCompatibilityPkg/Foundation/Framework/Include/ -IEdkCompatibilityPkg/Foundation/Include/ -IEdkCompatibilityPkg/Foundation/Efi -IEdkCompatibilityPkg/Foundation/Include/X64 -o e.dll e.c
	(dd bs=1 count=220 && perl -e 'printf "\xc"' && dd bs=1 count=1 of=/dev/null && cat) < e.dll > e.efi

.PHONY : clean
clean :
	rm -f d.dll d.efi e.dll e.efi
