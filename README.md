# UEFI Test Program #

The EdkCompatibilityPkg directory in edk2 is needed to make.

edk2: https://github.com/tianocore/edk2

### d.c ###

RnutimeServices Hook Test Program

This program is a UEFI runtime driver that can be loaded by the load command of the EFI Shell.
This program hooks RuntimeServices and records number of calls on an EFI variable named "callcount".

### e.c ###

RAM Disk Driver Test Program

This program is a UEFI runtime driver that can be loaded by the load command of the EFI Shell.
This program works as a RAM disk driver that provides a 4MiB RAM disk that initially contains 2 FAT partitions.
Use map -r command in the EFI Shell to update mappings.
