// RnutimeServices Hook Test Program
// Copyright 2014 Hideki EIRAKU

// Build with MinGW on GNU/Linux:
// amd64-mingw32msvc-gcc -shared -nostdlib -e EfiMain@8 -mno-red-zone -mno-sse -nostdinc -s -O -ffreestanding -fno-builtin -fno-stack-protector -fno-strict-aliasing -IEdkCompatibilityPkg/Foundation/Efi/Include/ -IEdkCompatibilityPkg/Foundation/Framework/Include/ -IEdkCompatibilityPkg/Foundation/Include/ -IEdkCompatibilityPkg/Foundation/Efi -IEdkCompatibilityPkg/Foundation/Include/X64 -o d.dll d.c
// (dd bs=1 count=220 && perl -e 'printf "\xc"' && dd bs=1 count=1 of=/dev/null && cat) < d.dll > d.efi
//
// Run from EFI Shell:
// load d.efi

#define EFI_SPECIFICATION_VERSION 0x00020000

#include <EfiCommon.h>
#include <EfiApi.h>

#define EVT_SIGNAL_VIRTUAL_ADDRESS_CHANGE 0x60000202

// a811dfe6-756a-11e4-9257-e33a1650dbb5
EFI_GUID CallCountGuid = { 0xa811dfe6, 0x756a, 0x11e4, { 0x92, 0x57, 0xe3, 0x3a, 0x16, 0x50, 0xdb, 0xb5 } };
CHAR16 CallCountName[] = L"callcount";
uint32_t CallCount[13];

EFI_GET_TIME                  OrigGetTime;
EFI_SET_TIME                  OrigSetTime;
EFI_GET_WAKEUP_TIME           OrigGetWakeupTime;
EFI_SET_WAKEUP_TIME           OrigSetWakeupTime;
EFI_SET_VIRTUAL_ADDRESS_MAP   OrigSetVirtualAddressMap;
EFI_GET_VARIABLE              OrigGetVariable;
EFI_GET_NEXT_VARIABLE_NAME    OrigGetNextVariableName;
EFI_SET_VARIABLE              OrigSetVariable;
EFI_GET_NEXT_HIGH_MONO_COUNT  OrigGetNextHighMonotonicCount;
EFI_RESET_SYSTEM              OrigResetSystem;
EFI_UPDATE_CAPSULE             OrigUpdateCapsule;
EFI_QUERY_CAPSULE_CAPABILITIES OrigQueryCapsuleCapabilities;
EFI_QUERY_VARIABLE_INFO        OrigQueryVariableInfo;

EFI_CONVERT_POINTER           OrigConvertPointer;

void
SaveOrigPointers (EFI_SYSTEM_TABLE *SystemTable)
{
  OrigGetTime = SystemTable->RuntimeServices->GetTime;
  OrigSetTime = SystemTable->RuntimeServices->SetTime;
  OrigGetWakeupTime = SystemTable->RuntimeServices->GetWakeupTime;
  OrigSetWakeupTime = SystemTable->RuntimeServices->SetWakeupTime;
  OrigSetVirtualAddressMap = SystemTable->RuntimeServices->SetVirtualAddressMap;
  OrigConvertPointer = SystemTable->RuntimeServices->ConvertPointer;
  OrigGetVariable = SystemTable->RuntimeServices->GetVariable;
  OrigGetNextVariableName = SystemTable->RuntimeServices->GetNextVariableName;
  OrigSetVariable = SystemTable->RuntimeServices->SetVariable;
  OrigGetNextHighMonotonicCount = SystemTable->RuntimeServices->GetNextHighMonotonicCount;
  OrigResetSystem = SystemTable->RuntimeServices->ResetSystem;
  OrigUpdateCapsule = SystemTable->RuntimeServices->UpdateCapsule;
  OrigQueryCapsuleCapabilities = SystemTable->RuntimeServices->QueryCapsuleCapabilities;
  OrigQueryVariableInfo = SystemTable->RuntimeServices->QueryVariableInfo;
}

EFI_STATUS
SaveCount (void)
{
  return OrigSetVariable (CallCountName, &CallCountGuid, EFI_VARIABLE_NON_VOLATILE | EFI_VARIABLE_BOOTSERVICE_ACCESS | EFI_VARIABLE_RUNTIME_ACCESS, sizeof CallCount, CallCount);
}

VOID EFIAPI
VirtualAddressChangeNotify (EFI_EVENT Event, VOID *Context)
{
  OrigConvertPointer (0, (VOID **)&OrigGetTime);
  OrigConvertPointer (0, (VOID **)&OrigSetTime);
  OrigConvertPointer (0, (VOID **)&OrigGetWakeupTime);
  OrigConvertPointer (0, (VOID **)&OrigSetWakeupTime);
  OrigConvertPointer (0, (VOID **)&OrigSetVirtualAddressMap);
  OrigConvertPointer (0, (VOID **)&OrigGetVariable);
  OrigConvertPointer (0, (VOID **)&OrigGetNextVariableName);
  OrigConvertPointer (0, (VOID **)&OrigSetVariable);
  OrigConvertPointer (0, (VOID **)&OrigGetNextHighMonotonicCount);
  OrigConvertPointer (0, (VOID **)&OrigResetSystem);
  OrigConvertPointer (0, (VOID **)&OrigUpdateCapsule);
  OrigConvertPointer (0, (VOID **)&OrigQueryCapsuleCapabilities);
  OrigConvertPointer (0, (VOID **)&OrigQueryVariableInfo);
}

EFI_RUNTIMESERVICE EFI_STATUS EFIAPI
HookGetTime (EFI_TIME *Time, EFI_TIME_CAPABILITIES *Capabilities)
{
  CallCount[0]++;
  SaveCount ();
  return OrigGetTime (Time, Capabilities);
}

EFI_RUNTIMESERVICE EFI_STATUS EFIAPI
HookSetTime (EFI_TIME *Time)
{
  CallCount[1]++;
  SaveCount ();
  return OrigSetTime (Time);
}

EFI_RUNTIMESERVICE EFI_STATUS EFIAPI
HookGetWakeupTime (BOOLEAN *Enabled, BOOLEAN *Pending, EFI_TIME *Time)
{
  CallCount[2]++;
  SaveCount ();
  return OrigGetWakeupTime (Enabled, Pending, Time);
}

EFI_RUNTIMESERVICE EFI_STATUS EFIAPI
HookSetWakeupTime (BOOLEAN Enable, EFI_TIME *Time)
{
  CallCount[3]++;
  SaveCount ();
  return OrigSetWakeupTime (Enable, Time);
}

EFI_RUNTIMESERVICE EFI_STATUS EFIAPI
HookSetVirtualAddressMap (UINTN MemoryMapSize, UINTN DescriptorSize, UINT32 DescriptorVersion, EFI_MEMORY_DESCRIPTOR *VirtualMap)
{
  CallCount[4]++;
  SaveCount ();
  return OrigSetVirtualAddressMap (MemoryMapSize, DescriptorSize, DescriptorVersion, VirtualMap);
}

EFI_RUNTIMESERVICE EFI_STATUS EFIAPI
HookGetVariable (CHAR16 *VariableName, EFI_GUID *VendorGuid, UINT32 *Attributes, UINTN *DataSize, VOID *Data)
{
  CallCount[5]++;
  SaveCount ();
  return OrigGetVariable (VariableName, VendorGuid, Attributes, DataSize, Data);
}

EFI_RUNTIMESERVICE EFI_STATUS EFIAPI
HookGetNextVariableName (UINTN *VariableNameSize, CHAR16 *VariableName, EFI_GUID *VendorGuid)
{
  CallCount[6]++;
  SaveCount ();
  return OrigGetNextVariableName (VariableNameSize, VariableName, VendorGuid);
}

EFI_RUNTIMESERVICE EFI_STATUS EFIAPI
HookSetVariable (CHAR16 *VariableName, EFI_GUID *VendorGuid, UINT32 Attributes, UINTN DataSize, VOID *Data)
{
  CallCount[7]++;
  SaveCount ();
  return OrigSetVariable (VariableName, VendorGuid, Attributes, DataSize, Data);
}

EFI_RUNTIMESERVICE EFI_STATUS EFIAPI
HookGetNextHighMonotonicCount (UINT32 *HighCount)
{
  CallCount[8]++;
  SaveCount ();
  return OrigGetNextHighMonotonicCount (HighCount);
}

EFI_RUNTIMESERVICE VOID EFIAPI
HookResetSystem (EFI_RESET_TYPE ResetType, EFI_STATUS ResetStatus, UINTN DataSize, CHAR16 *ResetData)
{
  CallCount[9]++;
  SaveCount ();
  return OrigResetSystem (ResetType, ResetStatus, DataSize, ResetData);
}

EFI_RUNTIMESERVICE EFI_STATUS EFIAPI
HookUpdateCapsule (EFI_CAPSULE_HEADER **CapsuleHeaderArray, UINTN CapsuleCount, EFI_PHYSICAL_ADDRESS ScatterGatherList)
{
  CallCount[10]++;
  SaveCount ();
  return OrigUpdateCapsule (CapsuleHeaderArray, CapsuleCount, ScatterGatherList);
}

EFI_RUNTIMESERVICE EFI_STATUS EFIAPI
HookQueryCapsuleCapabilities (EFI_CAPSULE_HEADER **CapsuleHeaderArray, UINTN CapsuleCount, UINT64 *MaxiumCapsuleSize, EFI_RESET_TYPE *ResetType)
{
  CallCount[11]++;
  SaveCount ();
  return OrigQueryCapsuleCapabilities (CapsuleHeaderArray, CapsuleCount, MaxiumCapsuleSize, ResetType);
}

EFI_RUNTIMESERVICE EFI_STATUS EFIAPI
HookQueryVariableInfo (UINT32 Attributes, UINT64 *MaximumVariableStorageSize, UINT64 *RemainingVariableStorageSize, UINT64 *MaximumVariableSize)
{
  CallCount[12]++;
  SaveCount ();
  return OrigQueryVariableInfo (Attributes, MaximumVariableStorageSize, RemainingVariableStorageSize, MaximumVariableSize);
}

void
InstallHooks (EFI_SYSTEM_TABLE *SystemTable)
{
  SystemTable->RuntimeServices->GetTime = HookGetTime;
  SystemTable->RuntimeServices->SetTime = HookSetTime;
  SystemTable->RuntimeServices->GetWakeupTime = HookGetWakeupTime;
  SystemTable->RuntimeServices->SetWakeupTime = HookSetWakeupTime;
  SystemTable->RuntimeServices->SetVirtualAddressMap = HookSetVirtualAddressMap;
  SystemTable->RuntimeServices->GetVariable = HookGetVariable;
  SystemTable->RuntimeServices->GetNextVariableName = HookGetNextVariableName;
  SystemTable->RuntimeServices->SetVariable = HookSetVariable;
  SystemTable->RuntimeServices->GetNextHighMonotonicCount = HookGetNextHighMonotonicCount;
  SystemTable->RuntimeServices->ResetSystem = HookResetSystem;
  SystemTable->RuntimeServices->UpdateCapsule = HookUpdateCapsule;
  SystemTable->RuntimeServices->QueryCapsuleCapabilities = HookQueryCapsuleCapabilities;
  SystemTable->RuntimeServices->QueryVariableInfo = HookQueryVariableInfo;
}

EFI_STATUS EFIAPI
EfiMain (EFI_HANDLE ImageHandle, EFI_SYSTEM_TABLE *SystemTable)
{
  SaveOrigPointers (SystemTable);
  EFI_EVENT Event;
  EFI_STATUS Status = SystemTable->BootServices->CreateEvent (EVT_SIGNAL_VIRTUAL_ADDRESS_CHANGE, EFI_TPL_NOTIFY, VirtualAddressChangeNotify, NULL, &Event);
  if (EFI_ERROR (Status))
    {
      SystemTable->ConOut->OutputString (SystemTable->ConOut, L"CreateEvent failed.\r\n");
      return Status;
    }
  InstallHooks (SystemTable);
  return EFI_SUCCESS;
}
